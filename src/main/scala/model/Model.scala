package model

import breeze.linalg.{DenseMatrix, DenseVector, convert, csvwrite}
import breeze.numerics.{log, sigmoid}
import breeze.stats.distributions.Rand
import org.apache.spark.SparkContext
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.rdd._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types._

import java.io.File
import scala.collection.mutable.ArrayBuffer
import scala.util.Random

/**
 * Model
 *
 * @param session - spark session
 */
class Model(
             var session: SparkSession,
             var emb_dim: Int = 50,
             var total_nodes: Int = 40334,
             var epochs: Int = 20,
             var learning_rate: Float = 0.0001F,
             var batch_size: Int = 10000,
             var negative_samples: Int = 20,
             var SEED: Int = 3,
             var verbose: Int = 2
           ) {
  val sc: SparkContext = session.sparkContext

  var trainDataPath = "hdfs:///epin/train_epin.csv"
  val outputSourceMatrixPath = "./source_matrix.csv"
  val outputTargetMatrixPath = "./target_matrix.csv"

  // Read data when method is instantiated
  var TRAIN_SET: RDD[(Int, Int)] = _
//    readData(trainDataPath)
  var TEST_SET: RDD[(Int, Int)] = _
//    readData(testDataPath)
  var EMBEDDINGS: (DenseMatrix[Float], DenseMatrix[Float]) = _
//    readEmbeddings((outputSourceMatrixPath, outputTargetMatrixPath))

  /**
   * train
   *
   * @return Embedding matrices
   */
  def train(): (DenseMatrix[Float], DenseMatrix[Float]) = {

    // Check the existence of a path
    if (TRAIN_SET == null) TRAIN_SET = Model.readData(session, trainDataPath)

    // Prints model settings
    printModelSettings()

    // create emb_src and emb_tgt
    val emb_src: DenseMatrix[Float] = createEmbeddingMatrix(emb_dim, total_nodes)
    val emb_tgt: DenseMatrix[Float] = createEmbeddingMatrix(emb_dim, total_nodes)
    val selected = TRAIN_SET.groupByKey().collectAsMap()

    val data = TRAIN_SET.sample(withReplacement = false, 0.0101, SEED)
    // create batches
    val batches: Array[RDD[(Int, Int)]] = prepareBatches(data, batch_size)

    // go over each epoch here, each time updating emb_src and emb_tgt and calculating loss
    for (epoch <- 1 to epochs) {
      println(s"${"-" * 40}\nEpoch $epoch/$epochs\n${"-" * 40}")
      val emb_src_broadcast = sc.broadcast(emb_src)
      val emb_tgt_broadcast = sc.broadcast(emb_tgt)

      // all cost scores for each batch of one epoch
      val J_cost = ArrayBuffer[Float]()

      // total iterations
      val total_iterations = batches.length

      for ((batch, index) <- batches.zipWithIndex) {
        val batchNo = index + 1
        println(s"Epoch $epoch, iteration $batchNo/$total_iterations")

        val batch_size = batch.count()

        val scaling_term = 1.0.toFloat / (batch_size * (negative_samples * batch_size + 1))

        val grads: RDD[((Int, DenseVector[Float]), (Int, DenseVector[Float]), (Int, Int))] =
          batch // has type RDD[(Int, Int)], i.e. RDD of edges
            .flatMap(edge => // define lambda
              Model.estimateGradientsForEdge(
                edge._1, // source node id
                edge._2, // destination node id
                emb_src_broadcast, // snapshot of emb_src
                emb_tgt_broadcast // snapshot of emb_tgt
                , selected(edge._1).toSet
              ) // has type RDD[(Int, DenseVector[Float])]
            )

        val src_grad_rdd = grads.map(_._1) // RDD[(Int, DenseVector)]
        val src_grads_local = src_grad_rdd // has non unique keys
          .reduceByKey(_ + _) // all keys are unique
          .collectAsMap() // download all the gradients to the driver and convert to HashMap (dictionary)

        val tgt_grad_rdd = grads.map(_._2)
        val tgt_grads_local = tgt_grad_rdd
          .reduceByKey(_ + _)
          .collectAsMap()

        // Updating weights of source nodes
        val src_grads_local_index = src_grads_local.keys.toIndexedSeq
        src_grads_local.zipWithIndex.map(a => {
          emb_src(::, src_grads_local_index(a._2)) := emb_src(::, src_grads_local_index(a._2)) - {
            src_grads_local(a._1._1) *:* learning_rate
          }
        })

        // Updating weights of target nodes
        val tgt_grads_local_index = tgt_grads_local.keys.toIndexedSeq
        tgt_grads_local.zipWithIndex.map(a => {
          emb_tgt(::, tgt_grads_local_index(a._2)) := emb_tgt(::, tgt_grads_local_index(a._2)) - {
            tgt_grads_local(a._1._1) *:* learning_rate //* scaling_term
          }
        })

        // Updating weights of target nodes
        if (verbose == 2) {
          // just negative samples for computing the right summation of J cost
          val negatives = grads.filter(_._3._1 == 0)

          // right summation of cost function J
          val right_summation = negatives.map(a => {
            val source = a._1._1
            val target = a._3._2
            val negative_target = a._2._1
            val value =
              log.logFloatImpl(
                1.0.toFloat - sigmoid.sigmoidImplFloat(emb_tgt_broadcast.value(::, negative_target).t * emb_src_broadcast.value(::, source))
              )
            ((source, target), value)
          })
            .groupByKey()

            .map(a => (a._1, a._2.sum))

          // J summation = left summation + right summation
          val J_summation: Float = batch.map(source_target => {
            val source = source_target._1
            val target = source_target._2
            val value = log.logFloatImpl(
              sigmoid.sigmoidImplFloat(emb_tgt_broadcast.value(::, target).t * emb_src_broadcast.value(::, source))
            )
            ((source, target), value)
          })
            .join(right_summation) // join by (source, target)
            .map(a => + a._2._1 + a._2._2)
            .reduce(_ + _)

          val J_batch: Float = - scaling_term * J_summation

          println(s" J_B in this batch $batchNo is $J_batch")
          J_cost.append(J_batch) // append the cost in this iteration to array J_cost
        }
      }
      if (verbose == 2) {
        val J_avg = J_cost.sum / batches.length // computes average cost
        println(s" J (cost) in epoch $epoch is $J_avg")
      }
    }

    println("Persisting the Model.....")
    println("Persisting Source Embedding Matrix .....")
    csvwrite(new File(outputSourceMatrixPath), convert(emb_src, Double), ',')
    println("Persisting Target Embedding Matrix .....")
    csvwrite(new File(outputTargetMatrixPath), convert(emb_tgt, Double), ',')

    EMBEDDINGS = (emb_src, emb_tgt)
    EMBEDDINGS // returns the weights
  }

  /**
   * prepareBatches
   * implement mini-batches for stochastic gradient descent
   *
   * @param data       - data
   * @param batch_size - batch size
   * @return batches of data
   */
  def prepareBatches(data: RDD[(Int, Int)], batch_size: Int): Array[RDD[(Int, Int)]] = {
    // Getting dataset size
    val dataset_size = data.count()

    // Computes the number of batches
    val batches_num = Math.ceil(dataset_size.toDouble / batch_size).toInt

    // Creates an index list for batches
    val batch_indices = (1 to batches_num).toList

    // data with its index zipped to it
    val zipped_data = data.zipWithIndex()

    // go through the indices then split
    val batches = batch_indices
      .map(
        batch_index => // batch index
          zipped_data // data with index
            .filter(
              data_index => {
                val start_index = batch_size * (batch_index - 1) // start index for one batch
                val end_index = start_index + batch_size // end index for one batch

                data_index._2 >= start_index && data_index._2 < end_index // specify the range of split
              }
            )
            .map(x => x._1) // keep just the data, so remove the index
      )
      .toArray

    batches
  }

  /**
   * getBatchesByRandomSplit
   * splits the dataset into batches of size _batch_size
   *
   * @param data        the dataset
   * @param _batch_size the batch size or batch fraction if has value in (0.0, 1.0)
   * @return the partitions of the dataset as an array of RDD
   */
  def getBatchesByRandomSplit(data: RDD[(Int, Int)], _batch_size: Int): Array[RDD[(Int, Int)]] = {
    // Getting dataset size
    val dataset_size = data.count()

    // Checks if the passed parameter is a fraction (0.0, 1.0) or size > 0
    val batch_frac =
      if (_batch_size >= 1 || _batch_size <= 0) _batch_size.toDouble / dataset_size.toDouble // size so convert to fraction
      else
        _batch_size // fraction so keep it

    // Computes the number of batches
    val batches_num = Math.ceil(dataset_size.toDouble / _batch_size).toInt

    // this array will contain the weights of each partition for splitting
    val splitArray = Array.fill(batches_num)(batch_frac.toDouble)

    // split the data according the array of weights
    val res = data.randomSplit(splitArray, seed = SEED)

    res
  }

  /**
   * Create embeddings/weights matrices
   *
   * @param embed_dim    the embedding dimension // number of weights
   * @param _total_nodes the number of nodes in the data
   * @param random_init  random or zero initialization of weights
   * @return the embedding matrix for all nodes as DenseMatrix[Float]
   */
  def createEmbeddingMatrix(embed_dim: Int, _total_nodes: Int, random_init: Boolean = true): DenseMatrix[Float] = {
    // If the random_init is true => initialize the weights with random numbers in the range (0.0, 1.0)
    if (random_init) {
      DenseMatrix.rand[Float](embed_dim, _total_nodes, rand = Rand.uniform.map(_.toFloat))
    } else { // else => initialize the weights with zeros
      DenseMatrix.zeros[Float](embed_dim, _total_nodes)
    }
  }

  def printModelSettings(): Unit ={
    println(this.toString)
  }

  override def toString: String = {
    s"""
      |Model Settings:
      |Training Data Path=$trainDataPath
      |Output Source Matrix Path=$outputSourceMatrixPath
      |Output Target Matrix Path=$outputTargetMatrixPath
      |Embeddings Dimensions=$emb_dim
      |Total Nodes=$total_nodes
      |Epochs=$epochs
      |Learning Rate=$learning_rate
      |Batch Size=$batch_size
      |Negative Samples=$negative_samples
      |Seed=$SEED
      |Verbose=$verbose
      |""".stripMargin
  }
}

/**
 * Model Object
 * contains static method (to prevent serialization error)
 */
object Model {

  val _negative_samples: Int = 20
  val _total_nodes: Int = 40334
  val set_all_nodes: scala.collection.immutable.Set[Int] = (0 to _total_nodes).toSet

  /**
   * estimateGradientsForEdge
   * estimate gradient for all edges. Note, K negative samples are generated randomly
   *
   * @param source        - source node
   * @param target        - target node
   * @param emb_src_broad - source broadcast matrix
   * @param emb_tgt_broad - target broadcast matrix
   * @return
   */
  def estimateGradientsForEdge(source: Int, target: Int,
                               emb_src_broad: Broadcast[DenseMatrix[Float]],
                               emb_tgt_broad: Broadcast[DenseMatrix[Float]],
                               targets_per_source: scala.collection.immutable.Set[Int]
                              ): Array[((Int, DenseVector[Float]), (Int, DenseVector[Float]), (Int, Int))] = {
    val emb_src = emb_src_broad.value // retrieve snapshot of emb_src
    val emb_tgt = emb_tgt_broad.value // retrieve snapshot of emb_tgt

    val src = emb_src(::, source)
    val tgt = emb_tgt(::, target)

    /*
     * Generate negative samples
     * Estimate gradients for positive and negative edges
     */
    val gradients = ArrayBuffer[((Int, DenseVector[Float]), (Int, DenseVector[Float]), (Int, Int))]()

    // calculate gradient for source and for target
    val grad_src = tgt * (sigmoid.sigmoidImplFloat(tgt.t * src) - 1.0.toFloat) // y => 1
    val grad_tgt = src * (sigmoid.sigmoidImplFloat(tgt.t * src) - 1.0.toFloat) // y => 1
    val grad_positive_edge = ((source, grad_src), (target, grad_tgt), (1, target))

    gradients.append(grad_positive_edge)

    val possible_negative_samples = set_all_nodes.diff(targets_per_source)

    val indexed_seq = Random.shuffle(possible_negative_samples).take(_negative_samples).toArray.toIndexedSeq

    // select an array of columns
    val rand_tgt = emb_tgt(::, indexed_seq).toDenseMatrix // matrix of weights of just chosen negative target nodes

    val negative_grads =
      indexed_seq.zipWithIndex.map(
        index => (
          (source, rand_tgt(::, index._2) *:* (sigmoid.sigmoidImplFloat(rand_tgt(::, index._2).t * src) - 0)),
          (index._2, src *:* (sigmoid.sigmoidImplFloat(rand_tgt(::, index._2).t * src) - 0)),
          (0, target) // label , real target for right summation of cost function J
        )
      )

    gradients.appendAll(negative_grads)

    gradients.toArray
  }

  /**
   * readData
   * read data from path
   *
   * @param path - path
   * @return the data nodes
   */
  def readData(session:SparkSession, path: String): RDD[(Int, Int)] = {
    session.read.format("csv")
      // the original data is store in CSV format
      // header: source_node, destination_node
      // here we read the data from CSV and export it as RDD[(Int, Int)],
      // i.e. as RDD of edges
      .option("header", "true")
      // State that the header is present in the file
      .schema(StructType(Array(
        StructField("source_node", IntegerType, nullable = false),
        StructField("destination_node", IntegerType, nullable = false)
      ))) // Define schema of the input data
      .load(path)
      // Read the file as DataFrame
      .rdd.map(row => (row.getAs[Int](0), row.getAs[Int](1)))
    // Interpret DF as RDD
  }

}
