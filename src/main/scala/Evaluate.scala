import Train.spinUpSpark
import breeze.linalg.{DenseMatrix, csvread}
import breeze.numerics.sigmoid
import model.Model
import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

import java.io.{File, FileNotFoundException, IOException}

object Evaluate {

  // Set Logger
  Logger.getLogger("org.apache.spark").setLevel(Level.ERROR)

  // Start Spark Engine
  val spark: SparkSession = spinUpSpark()
  val sc:SparkContext = spark.sparkContext

  val SEED:Int = 3

//  val testDataPath:String = "hdfs:///epin/test_epin.csv"
  var testDataPath:String = "./data/test_epin.csv"
  var outputSourceMatrixPath:String = "./source_matrix.csv"
  var outputTargetMatrixPath:String = "./target_matrix.csv"

  // Read data when method is instantiated
  var TRAIN_SET: RDD[(Int, Int)] = _
  //    readData(trainDataPath)
  var TEST_SET: RDD[(Int, Int)] = _
  //    readData(testDataPath)
  var EMBEDDINGS: (DenseMatrix[Float], DenseMatrix[Float]) = _
  var selected_random_node_size: Int = 100
  var top_rec_size:Int = 10
  var threshold:Float = 0.5F

  def main(args: Array[String]): Unit = {

    parseArgs(args)
    if (TEST_SET==null)  TEST_SET = Model.readData(spark, testDataPath)
    println("Reading Embeddings")
    EMBEDDINGS = readEmbeddings(paths = (outputSourceMatrixPath, outputTargetMatrixPath))
    println("Predicting....")
    val predictions = predict(selected_random_node_size, top_rec_size)
    predictions.foreach(println)
    println("Evaluating Model")
    val pred_rdd = sc.parallelize(predictions)
    val classification = pred_rdd.map(_._1)
      .leftOuterJoin(TEST_SET)
      .map(a=>((a._1,a._2._1),a._2._2))
      .join(pred_rdd)
      .map(a => (a._1._1, a._1._2 , if (a._2._2 > threshold) 1 else 0 ,
        {
          a._2._1 match {
            case Some(_) => 1 // positive class
            case _ => 0 // negative class
          }
        }
      )
      )

    val result = classification.collect()
    println("Source | Target | actual class | predicted class")
    result.map(a => s" ${a._1} | ${a._2} | ${a._3} |${a._4} ").foreach(println)

    println(s"Precision is ${100 * result.map(_._4).sum/result.length} %")

  }

  /**
   * readEmbeddings
   * read source and target embeddings
   *
   * @param paths - path to source and target embeddings
   * @return
   */
  def readEmbeddings(paths: (String, String)): (DenseMatrix[Float], DenseMatrix[Float]) = {
    var embeddings: (DenseMatrix[Float], DenseMatrix[Float]) = (null, null)

    try {
      val emb_src: DenseMatrix[Float] = csvread(new File(paths._1), ',').map(_.toFloat)
      val emb_tgt: DenseMatrix[Float] = csvread(new File(paths._2), ',').map(_.toFloat)

      embeddings = (emb_src, emb_tgt)
    } catch {
      case e: FileNotFoundException => println("Couldn't find embeddings files. Please train model => Model.train(...): " + e)
      case e: IOException => println("Had an IOException trying to read embeddings files: " + e)
    }

    embeddings
  }


  /**
   * predict
   *
   * @param random_node_size - random nodes in the graph, rate possible neighbors
   * @param top_rec_size     - top recommendations to get
   */
  def predict(random_node_size: Int = 100, top_rec_size: Int = 10): Array[((Int, Int), Float)] = {
    // check if model has been trained on data i.e embeddings exist
    if (EMBEDDINGS._1 == null || EMBEDDINGS._2 == null) {
      throw new RuntimeException("Looks like you've not trained this model yet. Use Model.train()")
    }

    // get random nodes from graph
    val random_samples: Array[(Int, Int)] = TEST_SET.takeSample(withReplacement = false, num = random_node_size, seed = SEED)

    val random_nodes: Array[Int] = random_samples.map(_._1).distinct // take just the source nodes except target nodes

    val rated_nodes = (EMBEDDINGS._2.t * EMBEDDINGS._1(::, random_nodes.toIndexedSeq))
  .map(a =>
    sigmoid.sigmoidImplFloat(a)
  ).activeIterator.toArray



    // filter nodes with edge in recommendation i.e make all edges in the recommendation unique
    val filtered_nodes: RDD[((Int, Int), Float)] = spark.sparkContext.parallelize(rated_nodes)
      .reduceByKey(math.max) // in cases of floating point prediction score small difference
      .distinct()
      .sortBy(k => k._2, ascending = false) // sort in descending order

    // take top recommendations
    filtered_nodes.collect().take(top_rec_size)
  }


  val testUsageString: String =
    s"""
      |Usage: spark-submit --master [local|yarn] --class Evaluate path/to/jar --path /path/to/training-data.csv --source-path /path/to/source-emb-matrix.csv --target-path /path/to/target-emb-matrix.csv --nodes 100 --recom-size 10 --threshold 0.5 --help
      |
      | Evaluate Model using the specified parameters
      |
      | -p, --path              Path of test dataset (default: hdfs:///epin/test_epin.csv)
      | -s, --source-path       Path of source embeddings matrix ( default: $outputSourceMatrixPath )
      | -t, --target-path       Path of target embeddings matrix ( default: $outputTargetMatrixPath )
      | -n, --nodes             Number of Random nodes selected for rating
      | -r, --recom-size        Number of recommendations to return
      | -d, --threshold         Threshold of classification ( default: 0.5 )
      | -h, --help              Shows this Help
      |""".stripMargin

  def parseArgs(args: Array[String]): Unit = {

    try {
      for ((arg, index) <- args.zipWithIndex) {
        if (arg.startsWith("-")) { // option
          val value = args(index + 1)
          arg.substring(1) match {
            case "p" | "-path" => testDataPath = value
            case "s" | "-source-path" => outputSourceMatrixPath = value
            case "t" | "-target-path" => outputTargetMatrixPath = value
            case "n" | "-nodes" => selected_random_node_size = value.toInt
            case "r" | "-recom-size" => top_rec_size = value.toInt
            case "d" | "-threshold" => threshold = value.toFloat
            case "h" | "-help" | _ =>
            println(testUsageString)
              sys.exit(0)
          }
        }
      }
    }
    catch {
      case _: Exception =>
      println(testUsageString)
        sys.exit(0)
    }finally {
      println(testUsageString)
    }
  }
}
