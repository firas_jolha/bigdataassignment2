import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession


object Main {
  // Set Logger
  Logger.getLogger("org.apache.spark").setLevel(Level.ERROR)

  def spinUpSpark(): SparkSession ={
    val spark:SparkSession = SparkSession
      .builder()
      .config("spark.driver.cores",4)
      .config("spark.executor.cores", 4)
      .appName("Scalable ML using Spark")
      .master("local[*]")
      .getOrCreate()

    spark
  }

  // Start Spark Engine
  val spark: SparkSession = spinUpSpark()

  def main(args: Array[String]): Unit = {
    println("For training Model ")
    println(Train.trainUsageString)
    println("For evaluating existing Model ")
    println(Evaluate.testUsageString)
  }


}
