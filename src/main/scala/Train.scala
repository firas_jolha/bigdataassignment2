import model.Model
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession

object Train {

  // Set Logger
  Logger.getLogger("org.apache.spark").setLevel(Level.ERROR)

  // Start Spark Engine
  val spark: SparkSession = spinUpSpark()


  def main(args: Array[String]): Unit = {

    val model = parseArgs(args)

    model.train()

  }

  def parseArgs(args: Array[String]): Model = {

    try {
      val model = new Model(Train.spark)
      for ((arg, index) <- args.zipWithIndex) {
        if (arg.startsWith("-")) { // option
          val value = args(index + 1)
          arg.substring(1) match {
            case "p" | "-path" => model.trainDataPath = value
            case "e" | "-epochs" => model.epochs = value.toInt
            case "l" | "-lr" => model.learning_rate = value.toFloat
            case "b" | "-batch-size" => model.batch_size = value.toInt
            case "n" | "-neg-samples" => model.negative_samples = value.toInt
            case "s" | "-seed" => model.SEED = value.toInt
            case "d" | "-embed" => model.emb_dim = value.toInt
            case "v" | "-verbose" => model.verbose = value.toInt
            case "h" | "-help" | _ =>
            println(trainUsageString)
              sys.exit(0)
          }
        }
      }
      model
    }
    catch {
      case e: Exception =>
      println(trainUsageString)
        sys.exit(0)
    }finally {
      println(trainUsageString)
    }
  }

  val trainUsageString: String =
    """
      |Usage: spark-submit --master [local|yarn] --class Train path/to/jar --path /path/to/training-data.csv --epochs 5 --lr 0.0001 --batch-size 10000 --neg-samples 20 --seed 3 --embed 50 --verbose 2 --help
      |
      | Train Model using the specified parameters
      |
      | -p, --path              Path of training dataset (default: hdfs:///epin/train_epin.csv)
      | -e, --epochs            Number of epochs to train the model
      | -l, --lr                Learning Rate of Mini-Batch Stochastic Gradient Descent
      | -b, --batch-size        Batch size for Mini-Batch Stochastic Gradient Descent
      | -n, --neg-samples       Number of negative samples that will be generated for each edge
      | -s, --seed              Seed of the random generator that used for initializing embeddings
      | -d, --embed             Size of embeddings/weights
      | -v, --verbose           Sets the verbose: ( 2 for computing J with updating weights in each iteration ), ( 1 for just updating weights without computing J )
      | -h, --help              Shows this Help
      |""".stripMargin

  def spinUpSpark(): SparkSession = {
    val spark: SparkSession = SparkSession
      .builder()
      .config("spark.driver.cores", 4)
      .config("spark.executor.cores", 4)
      .appName("Scalable ML using Spark")
      .master("local[*]")
      .getOrCreate()

    spark
  }
}


