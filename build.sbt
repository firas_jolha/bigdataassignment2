name := "BigDataAssignment2"

version := "0.1"

scalaVersion := "2.12.10"

//idePackagePrefix := Some("university.innopolis.bigdataassignment2")
val sparkVersion = "3.0.1"

libraryDependencies += "org.apache.spark" %% "spark-core" % sparkVersion
libraryDependencies += "org.apache.spark" %% "spark-sql" % sparkVersion
libraryDependencies += "org.apache.spark" %% "spark-mllib" % sparkVersion
libraryDependencies += "org.scalanlp" %% "breeze-config" % "0.9.2"


